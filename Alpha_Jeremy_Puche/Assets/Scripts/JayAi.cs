﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class JayAi : BasePlayer
{
    public enum EState
    {
        FetchEnemy,
    }

    protected override void InitStateMachine()
    {
        base.InitStateMachine();

        m_StateMachine.AddState(EState.FetchEnemy, StateMachine.TypeActionState.OnEnter, OnFetchEnemyEnter);
        m_StateMachine.AddState(EState.FetchEnemy, StateMachine.TypeActionState.Update, OnFetchEnemyUpdate);

      
        //m_Destination = GameManager.Instance.GetBallPos();
        m_StateMachine.SetState(EState.FetchEnemy);
    }


    // private float m_Counter;
    //private float m_DelayTimer;
    private Vector3 m_Direction;
    private Vector3 m_PlayerPos;
    // private EState m_currentState;

    protected override void Start()
    {
        base.Start();
        //GM = GameManager.Instance;

    }



    private void OnFetchEnemyEnter()
    {

        OnFetchEnemyUpdate();
        //m_currentState = EState.FetchEnemy;


    }
    private void OnFetchEnemyUpdate()
    {

        //for(int i = 0;i<GM.GetPlayerQty();i++)
        //{

        m_Direction = GameManager.Instance.GetPlayerPos();
        m_PlayerPos = GameManager.Instance.PlayerPos();
        Debug.Log(m_Direction);
        Debug.Log(m_PlayerPos);
        //}
        /* //if (IsStunned)
         //{
         //    ActivateShield();
         //}

         //if (!HasBall && !HasPowerUp)
         //{

         //    if (GM.GetPowerUpPositions().Count != 0f)

         //    {
         //        for (int i = 0; i < GM.GetPowerUpPositions().Count; i++)
         //        {
         //            if (Vector3.Distance(m_PlayerPos, GM.GetPowerUpPositions()[i]) <= 2.5f)
         //            {
         //                m_Direction = GM.GetPowerUpPositions()[i];
         //            }
         //        }
         //    }
         //}

         //m_PlayerPos = GM.GetPlayerPos(Id);
         //m_Direction = GM.GetBallPos();


         //if (HasBall)
         //{
         //    m_StateMachine.SetState(EState.BringBallBack);

         //}
         //if (GM.IsBallPickedUp)
         //{
         //    if (GM.PlayerWithBall != Id)
         //    {
         //        if (Vector3.Distance(GM.GetPlayerPos(GM.PlayerWithBall), GM.GetGoalPos(GM.PlayerWithBall)) <= 2f)
         //        {
         //            m_Direction = Vector3.zero;
         //        }
         //    }

         //}*/




        if (Vector3.Dot(transform.right, m_Direction - m_PlayerPos) <= 0)
        {
          
            //if (!GM.IsBallPickedUp)
            //{
            Move(EMoveDir.Backward);

            if (Vector3.Cross(transform.right, (m_Direction - m_PlayerPos).normalized).z > 0)
            {

                Turn(ETurnDir.Right);
            }
            else if (Vector3.Cross(transform.right, (m_Direction - m_PlayerPos).normalized).z < 0)
            {
                Turn(ETurnDir.Left);
            }
            //}
            else
            {
                for (int i = 0; i < GameManager.Instance.GetPlayerQty(); i++)
                {
                    if (i != Id)
                    {
                        //if (GM.GetPlayerStunStatus(i))

                        Move(EMoveDir.Backward);

                        if (Vector3.Cross(transform.right, (m_Direction - m_PlayerPos).normalized).z > 0)
                        {

                            Turn(ETurnDir.Right);
                        }
                        else if (Vector3.Cross(transform.right, (m_Direction - m_PlayerPos).normalized).z < 0)
                        {
                            Turn(ETurnDir.Left);
                        }

                        else
                        {
                            Move(EMoveDir.Forward);

                            if (Vector3.Cross(transform.right, (m_Direction - m_PlayerPos).normalized).z > 0)
                            {

                                Turn(ETurnDir.Left);
                            }
                            else if (Vector3.Cross(transform.right, (m_Direction - m_PlayerPos).normalized).z < 0)
                            {
                                Turn(ETurnDir.Right);
                            }

                        }
                    }

                }

            }
        }
        else
        {
            Move(EMoveDir.Forward);

            if (Vector3.Cross(transform.right, (m_Direction - m_PlayerPos).normalized).z > 0)
            {

                Turn(ETurnDir.Left);
            }
            else if (Vector3.Cross(transform.right, (m_Direction - m_PlayerPos).normalized).z < 0)
            {
                Turn(ETurnDir.Right);
            }
        }

    }




    //private void OnBringBallUpdate()
    //{

    //    if (IsStunned)
    //    {
    //        ActivateShield();
    //    }
    //    m_Direction = GM.GetGoalPos(Id);
    //    m_PlayerPos = GM.GetPlayerPos(Id);

    //    if (!HasBall)
    //    {
    //        m_StateMachine.SetState(EState.FetchBall);

    //    }
    //    if (Vector3.Dot(transform.right, m_Direction - m_PlayerPos) <= 0)
    //    {
    //        Move(EMoveDir.Backward);

    //        if (Vector3.Cross(transform.right, (m_Direction - m_PlayerPos).normalized).z > 0)
    //        {

    //            Turn(ETurnDir.Right);
    //        }
    //        else if (Vector3.Cross(transform.right, (m_Direction - m_PlayerPos).normalized).z < 0)
    //        {
    //            Turn(ETurnDir.Left);
    //        }

    //    }

    //    else
    //    {
    //        Move(EMoveDir.Forward);

    //        if (Vector3.Cross(transform.right, (m_Direction - m_PlayerPos).normalized).z > 0)
    //        {

    //            Turn(ETurnDir.Left);
    //        }
    //        else if (Vector3.Cross(transform.right, (m_Direction - m_PlayerPos).normalized).z < 0)
    //        {
    //            Turn(ETurnDir.Right);
    //        }
    //    }

    //}

    //private void OnBringBallEnter()
    //{

    //    m_Direction = GM.GetGoalPos(Id);
    //    m_currentState = EState.BringBallBack;

    //}



}
