﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Cheats : MonoBehaviour
{
    List<GameObject> Base;
    public GameObject CheatTable;
    public GameObject Base1;
    public GameObject Base2;
    public GameObject Base3;
    public GameObject Base4;

    public PlayerController player1;
    public PlayerController player2;
    public PlayerController player3;
    public PlayerController player4;

    public Toggle t_SuperSayan;
    public Toggle t_OneShot;
    public Toggle t_NoCol;
    public Toggle t_NoReload;




    private bool b_CheatsTable = false;
    private bool b_SuperSayan = false;
    private bool b_OneShot = false;
    private bool b_NoCol = false;
    private bool b_NoReload = false;

    public void Update()
    {

#if UNITY_CHEAT
        if (Input.GetKeyDown(KeyCode.C))
        {
            b_CheatsTable = !b_CheatsTable;
            if(CheatTable.activeInHierarchy)
            {
                ReturnToGame();
            }
            else
            {
                ActivateCheats();
            }
        }
      
        if (b_CheatsTable)
        {
            ActivateCheats();
            //Super sayan 
            if (Input.GetKeyDown(KeyCode.S))
            {
                b_SuperSayan = !b_SuperSayan;
                t_SuperSayan.isOn = !t_SuperSayan.isOn;

            }
            if (b_SuperSayan)
            {
                SuperSayan(true);
            }
            else
            {
                SuperSayan(false);
            }
            //Appuyer sur la touch espace pour redémarer la scene
            if (Input.GetKeyDown(KeyCode.G))
            {
                NewScene();
            }
            //one shot kill
            if (Input.GetKeyDown(KeyCode.O))
            {
                b_OneShot = !b_OneShot;
                t_OneShot.isOn = !t_OneShot.isOn;
            }
            if (b_OneShot)
            {
                OneShot(true);
            }
            else
            {
                OneShot(false);
            }
            //no collision
            if (Input.GetKeyDown(KeyCode.L))
            {
                b_NoCol = !b_NoCol;
                t_NoCol.isOn = !t_NoCol.isOn;
            }
            if (b_NoCol)
            {
                NoCol(true);
            }
            else
            {
                NoCol(false);
            }
            //no reload 
            if (Input.GetKeyDown(KeyCode.N))
            {
                b_NoReload = !b_NoReload;
                t_NoReload.isOn = !t_NoReload.isOn;
            }
            if (b_NoReload)
            {
                NoReload(true);
                Debug.Log("cheat actvated");
            }
            else
            {
                NoReload(false);

            }
            //next round
            if (Input.GetKeyDown(KeyCode.R))
            {
                NextRound();
            }
        }
     
    }
    public void ActivateCheats()
    {
        CheatTable.SetActive(true);
        Time.timeScale = 0f;
    }
    public void ReturnToGame()
    {
        CheatTable.SetActive(false);
        Time.timeScale = 1f;
    }
    public void SuperSayan(bool Value)
    {

        if (Value)
        {
            //PlayerController.Instance.m_Speed = 100f;
            //PlayerController.Instance.m_RotSpeed = 2000f;
            //PlayerController.Instance.m_Hp = 1000;
            player1.m_Speed = 100f;
            player1.m_RotSpeed = 200f;
            player1.m_Hp = 1000;

            player2.m_Speed = 100f;
            player2.m_RotSpeed = 200f;
            player2.m_Hp = 1000;

            /* player3.m_Speed = 100f;
             player3.m_RotSpeed = 200f;
             player3.m_Hp = 1000;

             player4.m_Speed = 100f;
             player4.m_RotSpeed = 200f;
             player4.m_Hp = 1000;*/
        }
        else
        {
            //PlayerController.Instance.m_Speed = 2f;
            //PlayerController.Instance.m_RotSpeed = 50f;
            //PlayerController.Instance.m_Hp = 3;
            player1.m_Speed = 2f;
            player1.m_RotSpeed = 50f;
            player1.m_Hp = 3;

            player2.m_Speed = 2f;
            player2.m_RotSpeed = 50f;
            player2.m_Hp = 3;

            /* player3.m_Speed = 2f;
             player3.m_RotSpeed = 50f;
             player3.m_Hp = 3;

             player4.m_Speed = 2f;
             player4.m_RotSpeed = 50f;
             player4.m_Hp = 3;*/
        }
    }
    public void NewScene()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name); // loads current scene
    }
    public void OneShot(bool Value)
    {
        if (Value)
        {
            //PlayerController.Instance.m_Damage = 1000;
            player1.m_Damage = 1000;
        }
        else
        {
            //PlayerController.Instance.m_Damage = 1;
            player1.m_Damage = 1;
        }
    }
    public void NoCol(bool Value)
    {
        if (Value)
        {
            Physics.IgnoreLayerCollision(13, 15, true);
            Physics.IgnoreLayerCollision(13, 14, true);
            Physics.IgnoreLayerCollision(13, 13, true);
            Physics.IgnoreLayerCollision(8, 9, true);
            Physics.IgnoreLayerCollision(8, 10, true);
            Physics.IgnoreLayerCollision(8, 11, true);
            Physics.IgnoreLayerCollision(9, 8, true);
            Physics.IgnoreLayerCollision(9, 10, true);
            Physics.IgnoreLayerCollision(9, 11, true);
            Physics.IgnoreLayerCollision(10, 8, true);
            Physics.IgnoreLayerCollision(10, 11, true);
            Physics.IgnoreLayerCollision(10, 9, true);
            Physics.IgnoreLayerCollision(11, 8, true);
            Physics.IgnoreLayerCollision(11, 9, true);
            Physics.IgnoreLayerCollision(11, 10, true);




            //Physics.IgnoreLayerCollision(LayerMask.GetMask("Ground"), LayerMask.GetMask("Base"), false);
            Base1.GetComponent<Rigidbody>().useGravity = false;
            Base2.GetComponent<Rigidbody>().useGravity = false;
            Base3.GetComponent<Rigidbody>().useGravity = false;
            Base4.GetComponent<Rigidbody>().useGravity = false;

        }
        else
        {
            Physics.IgnoreLayerCollision(13, 15, false);
            Physics.IgnoreLayerCollision(13, 14, false);
            Physics.IgnoreLayerCollision(8, 9, false);
            Physics.IgnoreLayerCollision(8, 10, false);
            Physics.IgnoreLayerCollision(8, 11, false);
            Physics.IgnoreLayerCollision(9, 8, false);
            Physics.IgnoreLayerCollision(9, 10, false);
            Physics.IgnoreLayerCollision(9, 11, false);
            Physics.IgnoreLayerCollision(10, 8, false);
            Physics.IgnoreLayerCollision(10, 11, false);
            Physics.IgnoreLayerCollision(10, 9, false);
            Physics.IgnoreLayerCollision(11, 8, false);
            Physics.IgnoreLayerCollision(11, 9, false);
            Physics.IgnoreLayerCollision(11, 10, false);
            //Physics.IgnoreLayerCollision(LayerMask.GetMask("Ground"), LayerMask.GetMask("Base"), false);
            Base1.GetComponent<Rigidbody>().useGravity = true;
            Base2.GetComponent<Rigidbody>().useGravity = true;
            Base3.GetComponent<Rigidbody>().useGravity = true;
            Base4.GetComponent<Rigidbody>().useGravity = true;
        }
    }
    public void NoReload(bool Value)
    {
        if (Value)
        {
            //PlayerController.Instance.cheatCanShoot = false;
            player1.cheatCanShoot = true;
            player2.cheatCanShoot = true;
            /* player3.cheatCanShoot = true;
             player4.cheatCanShoot = true;*/
        }
        else
        {
            //PlayerController.Instance.cheatCanShoot = true;
            player1.cheatCanShoot = false;
            player2.cheatCanShoot = false;
            /*player3.cheatCanShoot = false;
            player4.cheatCanShoot = false;*/

        }
    }
    public void NextRound()
    {
        ReturnToGame();
        CheatTable.SetActive(false);
        GameManager.Instance.playerOneWin();
       

    }
#endif
}


