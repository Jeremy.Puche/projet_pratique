﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileStart : MonoBehaviour {

   // public GameObject turret;
	
	//void Update () {
 //       transform.position = turret.transform.position;
 //       transform.eulerAngles = turret.transform.eulerAngles;
	//}

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Vector3 direction = transform.TransformDirection(Vector3.up) * 10;
        Gizmos.DrawRay(transform.position, direction);
    }
}
