﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour
{

    private void OnTriggerEnter(Collider aCol)
    {
        if (aCol.gameObject.tag == "Player1")
        {
            TutorialManager.Instance.CheckPoint();
        }
    }
}
