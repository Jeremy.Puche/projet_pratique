﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TutorialManager : MonoBehaviour
{

    public GameObject checkPoint1;
    public GameObject checkPoint2;
    public GameObject targets;
    public GameObject m_Player;
    public GameObject UiMove;
    public GameObject UiTurret;
    public GameObject UiShoot;

    public TextMeshProUGUI targetText;
    public int targetCount = 0;

    public CinemachineFreeLook cmFreeLook;

    [HideInInspector]
    public bool canShoot;
    [HideInInspector]
    public bool canMove;

    public static TutorialManager Instance
    {
        get; private set;
    }
    private void Awake()
    {
        targetText.text = "Target Destroyed: " + targetCount+"/3";
        Instance = this;
        m_Player.GetComponentInChildren<Turret>().enabled = false;
        m_Player.GetComponent<PlayerController>().cheatCanShoot = true;

        cmFreeLook.m_XAxis.m_MaxSpeed = 0f;
        cmFreeLook.m_YAxis.m_MaxSpeed = 0f;
    }

    private void OnTriggerEnter(Collider aCol)
    {

    }
    public void CheckPoint()
    {
        checkPoint2.SetActive(false);
        if (checkPoint1.gameObject.activeInHierarchy)
        {
            m_Player.GetComponentInChildren<Turret>().enabled = true;
            checkPoint1.SetActive(false);
            checkPoint2.SetActive(true);
            UiMove.SetActive(false);
            UiTurret.SetActive(true);
            cmFreeLook.m_XAxis.m_MaxSpeed = 200f;
            cmFreeLook.m_YAxis.m_MaxSpeed = 2f;
        }
        if (!checkPoint2.gameObject.activeInHierarchy && !checkPoint1.gameObject.activeInHierarchy)
        {
            m_Player.GetComponentInChildren<Turret>().enabled = true;
            targets.SetActive(true);
            checkPoint2.SetActive(false);
            UiMove.SetActive(false);
            UiTurret.SetActive(false);
            UiShoot.SetActive(true);
            m_Player.GetComponent<PlayerController>().cheatCanShoot = false;
        }
    }
}
