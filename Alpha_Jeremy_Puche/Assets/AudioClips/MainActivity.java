package ca.bart.minesweeper;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends Activity {


    public static final int COLS = 3;
    public static final int ROWS = 3;

    int[] grid = new int[COLS * ROWS];




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String appName = getString(R.string.app_name);


        for (int i = 0; i < COLS; ++i)
            for (int j = 0; j < ROWS; ++j)
            {

                final int x = i;
                final int y = j;
/*
                View v;
                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MainActivity.this.onClick(x, y);
                    }
                });
*/

            };


    }

    void onClick (int x, int y)
    {

    }

    void newGame()
    {

        for (int x = 0; x < COLS; ++x)
            for (int y = 0; y < ROWS; ++y)
            {
                grid[x + y * COLS] = 0;
            };


        // random

        refreshView();

    }

    boolean hasMine(int x, int y)
    {
        return (grid[x + y * COLS] & 1) == 1;
    }

    void setMine(int x, int y)
    {
        grid[x + y * COLS] |= 1;
    }

    void toggleFlag(int x, int y)
    {
        grid[x + y * COLS] ^= 4;
    }


    void refreshView()
    {
/*
        for (int x = 0; x < COLS; ++x)
            for (int y = 0; y < ROWS; ++y)
            {
                if (hasFlag(x, y))
                {
                    // has flag
                }
                else if (!isExposed(x, y))
                {
                    // idle
                }
                else
                {

                }


            };
*/

    }


}
