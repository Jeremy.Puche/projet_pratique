﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Targets : MonoBehaviour
{
    public GameObject explosion;

    private bool Death = false;
    private float m_Power = 350f;
    private float m_Time = 0f;
    private float m_Radius = 5.0f;

    private int targets = 0;

    private void OnTriggerEnter(Collider aCol)
    {
        //Debug.Log("target Hitted");
        // Debug.Log(aCol.gameObject.tag);

        if (aCol.gameObject.CompareTag("Bullet"))
        {
            Death = true;
            Vector3 explosionPos = transform.position;
            gameObject.GetComponent<Rigidbody>().AddExplosionForce(m_Power, explosionPos, m_Radius, 6.0F);
            explosion.GetComponent<ParticleSystem>().Play();
        }
    }
    private void Update()
    {
        if (Death)
        {
            targets += 1;
            m_Time += Time.deltaTime;
            if (m_Time >= 1.5f)
            {
                m_Time += Time.deltaTime;
                //Debug.Log(m_Time);
                Destroy(this.gameObject);
            }
        }
        targets = TutorialManager.Instance.targetCount;
    }
}
